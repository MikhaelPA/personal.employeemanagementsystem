﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Entity
{
    public class EmployeeDbContext : DbContext
    {
        public EmployeeDbContext(DbContextOptions<EmployeeDbContext> options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<UserAccess> UserAccesses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().ToTable("employee");

            modelBuilder.Entity<Employee>().Property(Q => Q.CreatedOn).HasDefaultValueSql("Date('Now')");

            modelBuilder.Entity<UserAccess>().ToTable("useraccess");

            modelBuilder.Entity<Employee>()
                .HasOne(Q => Q.UserAccess)
                .WithMany(Q => Q.Employees)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(Q => Q.CreatedBy)
                .HasConstraintName("FK_UserAccess_Employee");
        }
    }
}
