﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Employee.Entity
{
    public class Employee
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeIdentity { get; set; }

        /// <summary>
        /// No Karyawan.
        /// </summary>
        [Required]
        public string EmployeeID { get; set; }

        /// <summary>
        /// KTP/NIK
        /// </summary>
        [Required]
        public string NIK { get; set; }

        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Posisi Karyawan.
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// Tanggal lahir.
        /// </summary>
        public string DateOfBirth { get; set; }

        public string PlaceOfBirth { get; set; }

        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public string NPWP { get; set; }

        public string Religion { get; set; }
        public string EmergencyContactName { get; set; }

        public string EmergencyContactNo { get; set; }
        public bool IsActive { get; set; }

        public string CreatedOn { get; set; }

        [Required]
        public string CreatedBy { get; set; }

        public string UpdatedOn { get; set; }

        [ForeignKey("FK_UserAccess_Employee")]
        public UserAccess UserAccess { get; set; }
    }
}
