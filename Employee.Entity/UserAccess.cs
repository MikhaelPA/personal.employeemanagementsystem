﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Employee.Entity
{
    public class UserAccess
    {
        /// <summary>
        /// Username
        /// </summary>
        [Key]
        public string UserEmail { get; set; }

        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }

        public string AccessKey { get; set; }
        public bool HasAccess { get; set; }
        public string AccessGivenOn { get; set; }
        public string RequestedOn { get; set; }
        public List<Employee> Employees { get; set; }
    }
}
