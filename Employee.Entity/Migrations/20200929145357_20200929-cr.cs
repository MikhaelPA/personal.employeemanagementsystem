﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Employee.Entity.Migrations
{
    public partial class _20200929cr : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "useraccess",
                columns: table => new
                {
                    UserEmail = table.Column<string>(nullable: false),
                    Username = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false),
                    AccessKey = table.Column<string>(nullable: true),
                    HasAccess = table.Column<bool>(nullable: false),
                    AccessGivenOn = table.Column<string>(nullable: true),
                    RequestedOn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_useraccess", x => x.UserEmail);
                });

            migrationBuilder.CreateTable(
                name: "employee",
                columns: table => new
                {
                    EmployeeIdentity = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    EmployeeID = table.Column<string>(nullable: false),
                    NIK = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Position = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<string>(nullable: true),
                    PlaceOfBirth = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    NPWP = table.Column<string>(nullable: true),
                    Religion = table.Column<string>(nullable: true),
                    EmergencyContactName = table.Column<string>(nullable: true),
                    EmergencyContactNo = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<string>(nullable: true, defaultValueSql: "Date('Now')"),
                    CreatedBy = table.Column<string>(nullable: false),
                    UpdatedOn = table.Column<string>(nullable: true),
                    FK_UserAccess_Employee = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employee", x => x.EmployeeIdentity);
                    table.ForeignKey(
                        name: "FK_UserAccess_Employee",
                        column: x => x.CreatedBy,
                        principalTable: "useraccess",
                        principalColumn: "UserEmail",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_employee_CreatedBy",
                table: "employee",
                column: "CreatedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "employee");

            migrationBuilder.DropTable(
                name: "useraccess");
        }
    }
}
