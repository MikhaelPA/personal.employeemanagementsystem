﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Models
{
    public class ExcelModel
    {
        public ExcelModel(byte[] content, string contentType, string fileName)
        {
            Content = content;
            ContentType = contentType;
            FileName = fileName;
        }
        public byte[] Content { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
    }
}
