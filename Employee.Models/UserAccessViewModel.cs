﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Employee.Models
{
    public class UserAccessViewModel
    {
        [Display(Name = "Email")]
        public string UserEmail { get; set; }

        [Display(Name = "Username")]
        public string Username { get; set; }
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Display(Name = "Has Access")]
        public bool HasAccess { get; set; }

        [Display(Name = "Access Key")]
        public string AccessKey { get; set; }

        [Display(Name = "Access Given")]
        public string AccessGivenOn { get; set; }

        [Display(Name = "Requested")]
        public string RequestedOn { get; set; }
    }
}
