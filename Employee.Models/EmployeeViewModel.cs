﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Employee.Models
{
    public class EmployeeViewModel
    {
        /// <summary>
        /// Protected ID
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// No Karyawan.
        /// </summary>
        [Display(Name = "Employee ID")]
        [Required]
        public string EmployeeID { get; set; }

        /// <summary>
        /// KTP.
        /// </summary>
        [Display(Name = "NIK")]
        [Required]
        public string NIK { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        public string NPWP { get; set; }

        /// <summary>
        /// Posisi Karyawan.
        /// </summary>
        [Display(Name = "Job Position")]
        public string Position { get; set; }

        /// <summary>
        /// Tanggal lahir.
        /// </summary>
        [Display(Name = "Date of Birth")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Place of Birth")]
        public string PlaceOfBirth { get; set; }

        public string Address { get; set; }

        [Phone]
        [Required]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [EmailAddress]
        [Required]
        public string Email { get; set; }

        public string Religion { get; set; }
        [Display(Name = "Emergency Contact Name")]
        public string EmergencyContactName { get; set; }

        [Display(Name = "Emergency Contact No")]
        [Phone]
        public string EmergencyContactNo { get; set; }

        public string Status { get; set; }
        public bool IsActive { get; set; }
    }
}
