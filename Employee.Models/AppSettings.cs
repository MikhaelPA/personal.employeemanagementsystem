﻿namespace Employee.Models
{
    public class AppSettings
    {
        public string AdminKey { get; set; }
        public Email Email { get; set; }
    }

    public class Smtp
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool SSL { get; set; }
        public int Port { get; set; }
    }

    public class Email
    {
        public string Admin { get; set; }
        public Smtp Smtp { get; set; }
    }
}
