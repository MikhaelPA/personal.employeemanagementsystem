﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystem.Services
{
    public class HelperService
    {
        public List<SelectListItem> Religions
        {
            get
            {
                return new List<SelectListItem>{
                        new SelectListItem{ Value = "", Text = ""},
                        new SelectListItem{ Value = "Islam", Text = "Islam"},
                        new SelectListItem{ Value = "Kristen", Text = "Kristen"},
                        new SelectListItem{ Value = "Katolik", Text = "Katolik"},
                        new SelectListItem{ Value = "Buddha", Text = "Buddha"},
                        new SelectListItem{ Value = "Hindu", Text = "Hindu"},

                    };
            }
        }
    }
}
