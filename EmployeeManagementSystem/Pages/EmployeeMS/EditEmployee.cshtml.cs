using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Employee.Models;
using Employee.Services;
using EmployeeManagementSystem.Enums;
using EmployeeManagementSystem.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EmployeeManagementSystem.EmployeeMS.Pages
{
    [Authorize(Roles = UserRole.User)]
    [ValidateAntiForgeryToken]
    public class EditEmployeeModel : PageModel
    {
        private readonly EmployeeService _employeeService;

        private readonly HelperService _helperService;
        private readonly DataProtectionHelper _protectionHelper;
        public EditEmployeeModel(EmployeeService employeeService,
            HelperService helperService,
            DataProtectionHelper protectionHelper)
        {
            _employeeService = employeeService;
            _helperService = helperService;
            _protectionHelper = protectionHelper;
        }
        public List<SelectListItem> Religions => _helperService.Religions;
        [BindProperty]
        public EmployeeViewModel Employee { get; set; }
        public async Task<IActionResult> OnGetAsync(string ID)
        {
            ID = _protectionHelper.Decrypt(ID);
            var doesExist = await _employeeService.CheckEmployeeID(int.Parse(ID));
            if (doesExist == false)
            {
                return RedirectToPage("/Index");
            }
            Employee = await _employeeService.GetSelectedEmployeeAsync(int.Parse(ID));
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await _employeeService.UpdateEmployeeAsync(new Employee.Entity.Employee
            {
                EmployeeIdentity = int.Parse(_protectionHelper.Decrypt(Employee.ID)),
                EmployeeID = Employee.EmployeeID,
                Name = Employee.Name,
                NIK = Employee.NIK,
                Email = Employee.Email,
                PhoneNumber = Employee.PhoneNumber,
                Position = Employee.Position,
                Address = Employee.Address,
                Religion = Employee.Religion,
                DateOfBirth = Employee.DateOfBirth,
                PlaceOfBirth = Employee.PlaceOfBirth,
                EmergencyContactName = Employee.EmergencyContactName,
                EmergencyContactNo = Employee.EmergencyContactNo,
                NPWP = Employee.NPWP
            });

            return RedirectToPage("/Index");
        }
    }
}
