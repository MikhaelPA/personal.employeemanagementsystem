using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Employee.Models;
using Employee.Services;
using EmployeeManagementSystem.Enums;
using EmployeeManagementSystem.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EmployeeManagementSystem.EmployeeMS.Pages
{
    [Authorize(Roles = UserRole.User)]
    [ValidateAntiForgeryToken]
    public class NewEmployeeModel : PageModel
    {
        private readonly EmployeeService _employeeService;

        private readonly HelperService _helperService;
        public readonly DataProtectionHelper _protectionHelper;
        public NewEmployeeModel(EmployeeService employeeService,
            HelperService helperService,
            DataProtectionHelper protectionHelper)
        {
            _employeeService = employeeService;
            _helperService = helperService;
            _protectionHelper = protectionHelper;
        }
        public List<SelectListItem> Religions => _helperService.Religions;
        [BindProperty]
        public EmployeeViewModel Employee { get; set; }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await _employeeService.InsertEmployeeAsync(new Employee.Entity.Employee
            {
                EmployeeID = Employee.EmployeeID,
                Name = Employee.Name,
                NIK = Employee.NIK,
                Email = Employee.Email,
                PhoneNumber = Employee.PhoneNumber,
                Position = Employee.Position,
                Address = Employee.Address,
                Religion = Employee.Religion,
                DateOfBirth = Employee.DateOfBirth,
                PlaceOfBirth = Employee.PlaceOfBirth,
                EmergencyContactName = Employee.EmergencyContactName,
                EmergencyContactNo = Employee.EmergencyContactNo,
                NPWP = Employee.NPWP
        });

            return RedirectToPage("/Index");
        }
    }
}
