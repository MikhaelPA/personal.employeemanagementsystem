using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Employee.Models;
using Employee.Services;
using EmployeeManagementSystem.Enums;
using EmployeeManagementSystem.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EmployeeManagementSystem.Pages.Admin
{
    [Authorize(Roles = UserRole.Admin)]
    public class IndexModel : PageModel
    {
        private readonly UserAccessService _userAccessService;
        private readonly EmailService _emailService;

        public IndexModel(
            UserAccessService userAccessService,
            EmailService emailService)
        {
            _userAccessService = userAccessService;
            _emailService = emailService;
        }
        public string CurrentFilter { get; set; }
        public PaginatedList<UserAccessViewModel> Users;

        public async Task OnGetAsync(string currentFilter, int? pageIndex)
        {
            Users = await _userAccessService.GetUsersAsync(currentFilter, pageIndex);
        }

        public async Task<IActionResult> OnGetGiveAccessAsync(string email)
        {
            var accessKey = await _userAccessService.GiveUserAccessKeyAsync(email);
            
            //send email
            //var status = await _emailService.SendEmailAsync(email, accessKey, accessKey);
            //set session
            
            return RedirectToPage();
        }

        public async Task<IActionResult> OnGetDeleteAsync(string email)
        {
            await _userAccessService.DeleteUserAccessAsync(email);
            return RedirectToPage();
        }
    }
}
