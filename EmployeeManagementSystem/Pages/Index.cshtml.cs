﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Employee.Models;
using Employee.Services;
using EmployeeManagementSystem.Enums;
using EmployeeManagementSystem.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace EmployeeManagementSystem.Pages
{
    [Authorize]
    [ValidateAntiForgeryToken]
    public class IndexModel : PageModel
    {
        private readonly EmployeeService _employeeService;
        public readonly DataProtectionHelper _protectionHelper;
        private readonly ExcelService _excelService;

        public IndexModel(
            EmployeeService employeeService,
            DataProtectionHelper protectionHelper,
            ExcelService excelService)
        {
            _employeeService = employeeService;
            _protectionHelper = protectionHelper;
            _excelService = excelService;
        }

        public string CurrentFilter { get; set; }
        public PaginatedList<EmployeeViewModel> ActiveEmployees;

        public async Task<IActionResult> OnGetAsync(string currentFilter, int? pageIndex)
        {
            if (User.IsInRole(UserRole.Admin))
            {
                return RedirectToPage("/Admin/Index");
            }
            ActiveEmployees = await _employeeService.GetActiveEmployeeAsync(currentFilter, pageIndex);
            return Page();
        }

        public async Task<IActionResult> OnGetDeleteAsync(string ID)
        {
            ID = _protectionHelper.Decrypt(ID);
            await _employeeService.DeleteEmployeeAsync(int.Parse(ID));

            return RedirectToPage();
        }

        public async Task<IActionResult> OnGetExcelAsync()
        {
            var dataEmployee = await _employeeService.GetEmployeeExcelAsync();

            var excel = _excelService.DownloadExcelDocument(dataEmployee);

            return File(excel.Content, excel.ContentType, excel.FileName);
        }
    }
}
