using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Employee.Models;
using Employee.Services;
using EmployeeManagementSystem.Enums;
using EmployeeManagementSystem.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EmployeeManagementSystem.Pages.Auth
{
    public class LoginAccessKeyModel : PageModel
    {
        private readonly AppSettings _appSettings;
        private readonly UserAccessService _userAccessService;
        public LoginAccessKeyModel(AppSettings appSettings, 
            UserAccessService userAccessService)
        {
            _appSettings = appSettings;
            _userAccessService = userAccessService;
        }

        public IActionResult OnGet()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Index");
            }
            return Page();
        }

        [BindProperty]
        [Required]
        [Display(Name = "Access Key")]
        public string AccessKey { get; set; }

        [BindProperty]
        public string ErrorMessage { get; set; }

        public async Task<ActionResult> OnPostAsync()
        {
            var nameClaim = UserRole.Admin;
            var roleClaim = UserRole.Admin;
            // validate access key here
            if (AccessKey != _appSettings.AdminKey)
            {
                //check in UserAccess
                nameClaim = await _userAccessService.GetUserFromAccessKeyAsync(AccessKey);
                roleClaim = UserRole.User;

                ErrorMessage = "";
                if (string.IsNullOrEmpty(nameClaim))
                {
                    //write error message here
                    ErrorMessage = CustomMessage.EmailNotFound;
                    return Page();
                }
            }

            // register claims cookie
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, nameClaim),
                new Claim(ClaimTypes.Role, roleClaim)
            };

            var claimIdentity = new ClaimsIdentity(claims, CookieScheme.MyCookie);

            await HttpContext.SignInAsync(CookieScheme.MyCookie, new ClaimsPrincipal(claimIdentity));
            if (AccessKey == _appSettings.AdminKey)
            {
                //admin page
                return RedirectToPage("/Admin/Index");
            }
            return RedirectToPage("/Index");
        }

        public IActionResult OnGetRequestAccessAsync()
        {

            return RedirectToPage("/Auth/RequestAccess");
        }
    }
}
