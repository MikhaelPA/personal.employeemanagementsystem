using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Employee.Models;
using Employee.Services;
using EmployeeManagementSystem.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EmployeeManagementSystem.Pages.Auth
{
    [BindProperties]
    public class RegisterModel : PageModel
    {
        private readonly UserAccessService _userAccessService;
        private readonly EmailService _emailService;
        public RegisterModel(UserAccessService userAccessService,
            EmailService emailService)
        {
            _userAccessService = userAccessService;
            _emailService = emailService;
        }

        //public string Email { get; set; }

        public RegisterFormModel RegisterForm { get; set; }

        public bool IsShowMessage { get; set; }
        public string Message1
        {
            get
            {
                return CustomMessage.RequestSubmitted;
            }
        }
        public string Message2
        {
            get
            {
                return CustomMessage.RequestSubmittedEmailNotice;
            }
        }
        public string ErrorMessage { get; set; }

        public IActionResult OnGet()
        {
            IsShowMessage = false;
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Index");
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if(RegisterForm.Password != RegisterForm.ConfirmPassword)
            {
                ErrorMessage = CustomMessage.PasswordDoesNotMatch;
                return Page();
            }

            var isRegistered = await _userAccessService.IsEmailRegisteredAsync(RegisterForm.Email);
            var isUsernameUsed = await _userAccessService.IsUsernameAsync(RegisterForm.Username);
            if (isRegistered == false && isUsernameUsed == false)
            {
                //Save email in DB
                await _userAccessService.InsertUserAsync(RegisterForm.Email, RegisterForm.Username, RegisterForm.Password);
                IsShowMessage = true;
            }
            else if (isRegistered)
            {
                ErrorMessage = CustomMessage.EmailRegistered;
            }
            else if (isUsernameUsed)
            {
                ErrorMessage = CustomMessage.UserRegistered;
            }

            return Page();
        }
    }
}
