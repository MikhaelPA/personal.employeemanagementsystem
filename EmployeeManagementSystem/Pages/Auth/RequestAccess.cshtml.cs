using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Employee.Services;
using EmployeeManagementSystem.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EmployeeManagementSystem.Pages.Auth
{
    [BindProperties]
    public class RequestAccessModel : PageModel
    {
        private readonly UserAccessService _userAccessService;
        private readonly EmailService _emailService;
        public RequestAccessModel(UserAccessService userAccessService,
            EmailService emailService)
        {
            _userAccessService = userAccessService;
            _emailService = emailService;
        }

        public string Email { get; set; }
        public bool IsShowMessage { get; set; }
        public string Message1
        {
            get
            {
                return CustomMessage.RequestSubmitted;
            }
        }
        public string Message2
        {
            get
            {
                return CustomMessage.RequestSubmittedEmailNotice;
            }
        }
        public string ErrorMessage { get; set; }

        public IActionResult OnGet()
        {
            IsShowMessage = false;
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Index");
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var isRegistered = await _userAccessService.IsEmailRegisteredAsync(Email);
            if (isRegistered == false)
            {
                //Save email in DB
                //await _userAccessService.InsertUserAccessAsync(Email);
                IsShowMessage = true;
            }
            else if (await _userAccessService.IsEmailHasAccessAsync(Email))
            {
                //await _userAccessService.UpdateAccessKeyAsync(Email);
                ErrorMessage = CustomMessage.EmailRegistered;
                //send email
                //var status = await _emailService.SendEmailAsync(Email, accessKey, accessKey);
                //set session
                //if(status == false)
                //{
                //    ErrorMessage = CustomMessage.FailToSendEmail;
                //}
            }

            return Page();
        }
    }
}
