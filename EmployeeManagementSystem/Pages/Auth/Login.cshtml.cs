using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Employee.Models;
using Employee.Services;
using EmployeeManagementSystem.Enums;
using EmployeeManagementSystem.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EmployeeManagementSystem.Pages.Auth
{
    public class LoginModel : PageModel
    {
        private readonly AppSettings _appSettings;
        private readonly UserAccessService _userAccessService;
        private readonly DataProtectionHelper _protector;

        public LoginModel(AppSettings appSettings, 
            UserAccessService userAccessService,
            DataProtectionHelper protector)
        {
            _appSettings = appSettings;
            _userAccessService = userAccessService;
            _protector = protector;
        }

        public IActionResult OnGet()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Index");
            }
            return Page();
        }

        [BindProperty]
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }
        [BindProperty]
        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [BindProperty]
        public string ErrorMessage { get; set; }

        public async Task<ActionResult> OnPostAsync()
        {
            var nameClaim = UserRole.Admin;
            var emailClaim = UserRole.Admin;
            var roleClaim = UserRole.Admin;
            // validate access key here
            if (Username != _appSettings.AdminKey && Password != _appSettings.AdminKey)
            {
                //check in UserAccess
                emailClaim = await _userAccessService.GetUserFromUsernamePasswordAsync(Username,  Password);
                nameClaim = Username;
                roleClaim = UserRole.User;

                ErrorMessage = "";
                if (string.IsNullOrEmpty(emailClaim))
                {
                    //write error message here
                    ErrorMessage = CustomMessage.UserNotFound;
                    return Page();
                }
                if(await _userAccessService.IsEmailHasAccessAsync(_protector.Decrypt(emailClaim)) == false)
                {
                    ErrorMessage = CustomMessage.AccessDenied;
                    return Page();
                }
            }

            // register claims cookie
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, nameClaim),
                new Claim(ClaimTypes.Email, emailClaim),
                new Claim(ClaimTypes.Role, roleClaim)
            };

            var claimIdentity = new ClaimsIdentity(claims, CookieScheme.MyCookie);

            await HttpContext.SignInAsync(CookieScheme.MyCookie, new ClaimsPrincipal(claimIdentity));
            if (Username == _appSettings.AdminKey)
            {
                //admin page
                return RedirectToPage("/Admin/Index");
            }
            return RedirectToPage("/Index");
        }

        public IActionResult OnGetRequestAccessAsync()
        {

            return RedirectToPage("/Auth/Register");
        }
    }
}
