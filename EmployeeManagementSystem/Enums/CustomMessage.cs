﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystem.Enums
{
    internal static class CustomMessage
    {
        public const string EmailNotFound = "Please insert a valid access key.";
        public const string EmailRegistered = "This Email address is registered.";

        public const string UserNotFound = "Incorrect username or password.";
        public const string UserRegistered = "This Username is registered.";
        public const string PasswordDoesNotMatch = "Password doesn't match.";

        public const string AccessDenied = "Your access has not been approved.";

        public const string FailToSendEmail = "Failed to send Email.";
        public const string RequestSubmitted = "Your request has been submitted.";
        public const string RequestSubmittedEmailNotice = "The administrator will review your request soon.";
    }
}
