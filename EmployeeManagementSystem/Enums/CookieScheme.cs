﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystem.Enums
{
    internal static class CookieScheme
    {
        public const string MyCookie = "Elmiel.Cookie-Scheme.v1";
    }
}
