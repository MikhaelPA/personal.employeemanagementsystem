using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Employee.Entity;
using Employee.Models;
using Employee.Services;
using EmployeeManagementSystem.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace EmployeeManagementSystem
{
    public class Startup
    {
        private readonly string CookieAuthScheme = "Elmiel.Cookie-Scheme.v1";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddDataProtection()
                .PersistKeysToFileSystem(new DirectoryInfo(@"keys"));
            services.AddDbContext<EmployeeDbContext>(options =>
            {
                options.UseSqlite("Data Source=employeems.db");
            });

            services.AddAuthentication(CookieAuthScheme)
                .AddCookie(CookieAuthScheme, opt =>
                {
                    opt.LoginPath = "/Auth/Login";
                    opt.LogoutPath = "/Auth/Logout";
                    opt.AccessDeniedPath = "/Auth/Denied";
                    opt.ExpireTimeSpan = TimeSpan.FromHours(18);
                });

            services.AddTransient<EmployeeService>();
            services.AddTransient<UserAccessService>();
            services.AddSingleton<HelperService>();
            services.AddSingleton<DataProtectionHelper>();
            services.AddTransient<ExcelService>();
            services.AddTransient<EmailService>();

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            ConfigureAppsettings(services);

            services.AddTransient<SmtpClient>(serviceProvider =>
            {
                return new SmtpClient()
                {
                    Host = Configuration.GetValue<string>("Email:Smtp:Host"),
                    Port = Configuration.GetValue<int>("Email:Smtp:Port"),
                    Credentials = new NetworkCredential(
                            Configuration.GetValue<string>("Email:Smtp:Username"),
                            Configuration.GetValue<string>("Email:Smtp:Password")
                    ),
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = true
                };
            });
        }

        public void ConfigureAppsettings(IServiceCollection services)
        {
            services.Configure<AppSettings>(Configuration);
            services.AddScoped(di => di.GetService<IOptions<AppSettings>>().Value);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            var scope = app.ApplicationServices.CreateScope();

            scope.ServiceProvider.GetRequiredService<EmployeeDbContext>().Database.Migrate();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
