﻿using Employee.Entity;
using Employee.Models;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Services
{
    public class UserAccessService
    {
        private readonly EmployeeDbContext _employeeDbContext;
        private readonly DataProtectionHelper _protector;
        public UserAccessService(EmployeeDbContext employeeDbContext,
            DataProtectionHelper protectionHelper)
        {
            _employeeDbContext = employeeDbContext;
            _protector = protectionHelper;
        }

        /// <summary>
        /// Validate access key.
        /// </summary>
        /// <param name="accessKey"></param>
        /// <returns>User Email</returns>
        public async Task<string> GetUserFromAccessKeyAsync(string accessKey)
        {

            return await _employeeDbContext.UserAccesses.AsNoTracking()
                .Where(Q => Q.HasAccess && Q.AccessKey == accessKey)
                .Select(Q => _protector.Encrypt(Q.UserEmail))
                .FirstOrDefaultAsync();
        }

        public async Task<string> GetUserFromUsernamePasswordAsync(string username, string password)
        {
            return await _employeeDbContext.UserAccesses.AsNoTracking()
                .Where(Q => Q.Username == username && Q.Password == password)
                .Select(Q => _protector.Encrypt(Q.UserEmail))
                .FirstOrDefaultAsync();
        }

        public async Task<bool> IsEmailRegisteredAsync(string email)
        {
            return await _employeeDbContext.UserAccesses.AsNoTracking()
                .AnyAsync(Q => Q.UserEmail == email);
        }

        public async Task<bool> IsUsernameAsync(string username)
        {
            return await _employeeDbContext.UserAccesses.AsNoTracking()
                .AnyAsync(Q => Q.Username == username);
        }

        public async Task<bool> IsEmailHasAccessAsync(string email)
        {
            return await _employeeDbContext.UserAccesses.AsNoTracking()
                .AnyAsync(Q => Q.UserEmail == email && Q.HasAccess);
        }


        public async Task InsertUserAsync(string email, string username, string password)
        {
            try
            {

                await _employeeDbContext.UserAccesses.AddAsync(new Entity.UserAccess
                {
                    UserEmail = email,
                    Username = username,
                    Password = password,
                    HasAccess = false,
                    RequestedOn = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")
                });

                await _employeeDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Give access to user.
        /// </summary>
        /// <param name="email"></param>
        /// <returns>GUID - Access Key</returns>
        public async Task<string> GiveUserAccessKeyAsync(string email)
        {
            var accessKey = Guid.NewGuid().ToString();
            var toUpdate = await _employeeDbContext.UserAccesses
                    .Where(Q => Q.UserEmail == email).FirstOrDefaultAsync();

            toUpdate.HasAccess = true;
            toUpdate.AccessGivenOn = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            toUpdate.AccessKey = accessKey;

            _employeeDbContext.UserAccesses.Update(toUpdate);
            try
            {
                await _employeeDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return accessKey;
        }

        public async Task<string> UpdateAccessKeyAsync(string email)
        {
            var accessKey = Guid.NewGuid().ToString();
            var toUpdate = await _employeeDbContext.UserAccesses
                    .Where(Q => Q.UserEmail == email).FirstOrDefaultAsync();

            toUpdate.AccessGivenOn = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            toUpdate.AccessKey = accessKey;

            _employeeDbContext.UserAccesses.Update(toUpdate);
            try
            {
                await _employeeDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return accessKey;
        }

        public async Task<PaginatedList<UserAccessViewModel>> GetUsersAsync(string currentFilter, int? pageIndex)
        {

            var query = _employeeDbContext.UserAccesses.AsNoTracking().Select(Q => new UserAccessViewModel
            {
                UserEmail = Q.UserEmail,
                RequestedOn = Q.RequestedOn,
                Username = Q.Username,
                Password = Q.Password,
                AccessKey = Q.AccessKey,
                AccessGivenOn = Q.AccessGivenOn,
                HasAccess = Q.HasAccess
            });
            if (string.IsNullOrEmpty(currentFilter) == false)
            {
                pageIndex = 1;
                query = query.Where(Q => Q.UserEmail.ToLower().Contains(currentFilter.ToLower()));
            }

            var pageSize = 10;

            return await PaginatedList<UserAccessViewModel>.CreateAsync(
                query.OrderBy(Q => Q.RequestedOn), pageIndex ?? 1, pageSize);

        }

        public async Task DeleteUserAccessAsync(string email)
        {
            var toDelete = await _employeeDbContext.UserAccesses
                    .Where(Q => Q.UserEmail == email).FirstOrDefaultAsync();

            _employeeDbContext.UserAccesses.Remove(toDelete);
            try
            {
                await _employeeDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
