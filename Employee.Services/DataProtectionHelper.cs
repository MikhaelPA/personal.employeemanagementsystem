﻿using Microsoft.AspNetCore.DataProtection;

namespace Employee.Services
{
    public class DataProtectionHelper
    {
        private readonly IDataProtectionProvider
            _dataProtectionProvider;
        public DataProtectionHelper(IDataProtectionProvider
            dataProtectionProvider)
        {
            _dataProtectionProvider = dataProtectionProvider;
        }
        public string Encrypt(string textToEncrypt)
        {
            return _dataProtectionProvider.CreateProtector("EmployeeMS.Elmiel.v1").
            Protect(textToEncrypt);
        }
        public string Decrypt(string cipherText)
        {
            return _dataProtectionProvider.CreateProtector("EmployeeMS.Elmiel.v1").
            Unprotect(cipherText);
        }
    }
}
