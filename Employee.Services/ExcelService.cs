﻿using ClosedXML.Excel;
using Employee.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Employee.Services
{
    public class ExcelService
    {
        public ExcelModel DownloadExcelDocument(List<EmployeeViewModel> employees)
        {
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            string fileName = $"{DateTime.Now:yyyyMMdd_HHmmss_}employee.xlsx";

            using var workbook = new XLWorkbook(XLEventTracking.Disabled);
            IXLWorksheet worksheet = workbook.Worksheets.Add("Employee");
            worksheet.Cell(1, 1).Value = "Employee ID";
            worksheet.Cell(1, 2).Value = "Name";
            worksheet.Cell(1, 3).Value = "NIK";
            worksheet.Cell(1, 4).Value = "Email";
            worksheet.Cell(1, 5).Value = "Phone Number";
            worksheet.Cell(1, 6).Value = "NPWP";
            worksheet.Cell(1, 7).Value = "Address";
            worksheet.Cell(1, 8).Value = "Place of Birth";
            worksheet.Cell(1, 9).Value = "Date of Birth";
            worksheet.Cell(1, 10).Value = "Job Position";
            worksheet.Cell(1, 11).Value = "Religion";
            worksheet.Cell(1, 12).Value = "Emergency Contact Name";
            worksheet.Cell(1, 13).Value = "Emergency Contact No";

            worksheet.Cells(true).Style.Fill.BackgroundColor = XLColor.LightSkyBlue;

            for (int index = 1; index <= employees.Count; index++)
            {
                worksheet.Cell(index + 1, 1).Value = "'" + employees[index - 1].EmployeeID;
                worksheet.Cell(index + 1, 2).Value = employees[index - 1].Name;
                worksheet.Cell(index + 1, 3).Value = "'" + employees[index - 1].NIK;
                worksheet.Cell(index + 1, 4).Value = employees[index - 1].Email;
                worksheet.Cell(index + 1, 5).Value = "'" + employees[index - 1].PhoneNumber;
                worksheet.Cell(index + 1, 6).Value = "'" + employees[index - 1].NPWP;
                worksheet.Cell(index + 1, 7).Value = employees[index - 1].Address;
                worksheet.Cell(index + 1, 8).Value = employees[index - 1].PlaceOfBirth;
                worksheet.Cell(index + 1, 9).Value = employees[index - 1].DateOfBirth;
                worksheet.Cell(index + 1, 10).Value = employees[index - 1].Position;
                worksheet.Cell(index + 1, 11).Value = employees[index - 1].Religion;
                worksheet.Cell(index + 1, 12).Value = employees[index - 1].EmergencyContactName;
                worksheet.Cell(index + 1, 13).Value = "'" + employees[index - 1].EmergencyContactNo;

                for (var col = 1; col <= 13; col++) {
                    worksheet.Cell(index + 1, col).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(index + 1, col).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(index + 1, col).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(index + 1, col).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                }
            }

            //adjustment style
            worksheet.Columns().AdjustToContents();
            worksheet.RangeUsed().SetAutoFilter();
            //worksheet.Row(1).Style.Fill.BackgroundColor = XLColor.LightSkyBlue;
            using var stream = new MemoryStream();
            workbook.SaveAs(stream);
            var content = stream.ToArray();
            return new ExcelModel(content, contentType, fileName);

        }
    }
}
