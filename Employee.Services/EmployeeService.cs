﻿using Employee.Entity;
using Employee.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Services
{
    public class EmployeeService
    {
        private readonly EmployeeDbContext _employeeDbContext;
        private readonly DataProtectionHelper _protector;
        private readonly IHttpContextAccessor _accessor;
        private readonly string User;

        public EmployeeService(EmployeeDbContext dbContext, DataProtectionHelper protector,
            IHttpContextAccessor httpContext)
        {

            _employeeDbContext = dbContext;
            _protector = protector;
            _accessor = httpContext;
            User = _protector.Decrypt(_accessor.HttpContext.User.FindFirstValue(ClaimTypes.Email));
        }

        /// <summary>
        /// Get Employee Data.
        /// </summary>
        /// <returns></returns>
        public async Task<PaginatedList<EmployeeViewModel>> GetActiveEmployeeAsync(string currentFilter, int? pageIndex)
        {
            
            var query = _employeeDbContext.Employees.AsNoTracking()
                .Where(Q => Q.IsActive && User == Q.CreatedBy)
                .Select(Q => new EmployeeViewModel
                {
                    ID = _protector.Encrypt(Q.EmployeeIdentity.ToString()),
                    Name = Q.Name,
                    EmployeeID = _protector.Encrypt(Q.EmployeeID),
                    Position = Q.Position,
                    NIK = Q.NIK,
                    DateOfBirth = Q.DateOfBirth,
                    PlaceOfBirth = Q.PlaceOfBirth,
                    Address = Q.Address,
                    Email = Q.Email,
                    PhoneNumber = Q.PhoneNumber,
                    NPWP = Q.NPWP,
                    Religion = Q.Religion,
                    EmergencyContactName = Q.EmergencyContactName,
                    EmergencyContactNo = Q.EmergencyContactNo,
                    IsActive = Q.IsActive,
                    Status = Q.IsActive ? "Aktif" : "Tidak Aktif"
                });

            if (string.IsNullOrEmpty(currentFilter) == false)
            {
                pageIndex = 1;
                query = query.Where(Q => Q.Name.ToLower().Contains(currentFilter.ToLower()));
            }

            var pageSize = 10;

            return await PaginatedList<EmployeeViewModel>.CreateAsync(
                query.OrderBy(Q => Q.Name), pageIndex ?? 1, pageSize);
        }

        /// <summary>
        /// Get Employee Data.
        /// </summary>
        /// <returns></returns>
        public async Task<List<EmployeeViewModel>> GetEmployeeExcelAsync()
        {
            
            return await _employeeDbContext.Employees.AsNoTracking()
                .Where(Q => Q.IsActive && User == Q.CreatedBy)
                .Select(Q => new EmployeeViewModel
                {
                    Name = Q.Name,
                    EmployeeID = Q.EmployeeID,
                    Position = Q.Position,
                    NIK = Q.NIK,
                    DateOfBirth = Q.DateOfBirth,
                    PlaceOfBirth = Q.PlaceOfBirth,
                    Address = Q.Address,
                    Email = Q.Email,
                    PhoneNumber = Q.PhoneNumber,
                    NPWP = Q.NPWP,
                    Religion = Q.Religion,
                    EmergencyContactName = Q.EmergencyContactName,
                    EmergencyContactNo = Q.EmergencyContactNo,
                }).OrderBy(Q => Q.Name).ToListAsync();
        }

        public async Task<bool> CheckEmployeeID(int id)
        {
            return await _employeeDbContext.Employees.AsNoTracking().AnyAsync(Q => Q.EmployeeIdentity == id);
        }

        public async Task<EmployeeViewModel> GetSelectedEmployeeAsync(int id)
        {
            return await _employeeDbContext.Employees.AsNoTracking()
                .Where(Q => Q.EmployeeIdentity == id)
                .Select(Q => new EmployeeViewModel
                {
                    ID = _protector.Encrypt(Q.EmployeeIdentity.ToString()),
                    EmployeeID = Q.EmployeeID,
                    Name = Q.Name,
                    Position = Q.Position,
                    NIK = Q.NIK,
                    DateOfBirth = Q.DateOfBirth,
                    PlaceOfBirth = Q.PlaceOfBirth,
                    Address = Q.Address,
                    Email = Q.Email,
                    PhoneNumber = Q.PhoneNumber,
                    NPWP = Q.NPWP,
                    Religion = Q.Religion,
                    EmergencyContactName = Q.EmergencyContactName,
                    EmergencyContactNo = Q.EmergencyContactNo,
                    IsActive = Q.IsActive,
                    Status = Q.IsActive ? "Aktif" : "Tidak Aktif"
                }).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Insert Employee Data.
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public async Task InsertEmployeeAsync(Entity.Employee employee)
        {
            try
            {
                await _employeeDbContext.Employees.AddAsync(new Entity.Employee
                {
                    EmployeeID = employee.EmployeeID,
                    Name = employee.Name,
                    NIK = employee.NIK,
                    Email = employee.Email,
                    PhoneNumber = employee.PhoneNumber,
                    Position = employee.Position,
                    Address = employee.Address,
                    Religion = employee.Religion,
                    DateOfBirth = employee.DateOfBirth,
                    PlaceOfBirth = employee.PlaceOfBirth,
                    EmergencyContactName = employee.EmergencyContactName,
                    EmergencyContactNo = employee.EmergencyContactNo,
                    NPWP = employee.NPWP,
                    CreatedBy = User,
                    CreatedOn = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"),
                    IsActive = true
                });

                await _employeeDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task UpdateEmployeeAsync(Entity.Employee employee)
        {
            try
            {
                var toUpdate = await _employeeDbContext.Employees.FirstOrDefaultAsync(Q => Q.EmployeeIdentity == employee.EmployeeIdentity);

                toUpdate.EmployeeID = employee.EmployeeID;
                toUpdate.Name = employee.Name;
                toUpdate.DateOfBirth = employee.DateOfBirth;
                toUpdate.NIK = employee.NIK;
                toUpdate.Position = employee.Position;
                toUpdate.PlaceOfBirth = employee.PlaceOfBirth;
                toUpdate.Email = employee.Email;
                toUpdate.PhoneNumber = employee.PhoneNumber;
                toUpdate.Address = employee.Address;
                toUpdate.Religion = employee.Religion;
                toUpdate.EmergencyContactName = employee.EmergencyContactName;
                toUpdate.EmergencyContactNo = employee.EmergencyContactNo;
                toUpdate.NPWP = employee.NPWP;
                toUpdate.UpdatedOn = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");

                _employeeDbContext.Employees.Update(toUpdate);

                await _employeeDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task DeleteEmployeeAsync(int id)
        {
            try
            {
                var toBeDeleted = await _employeeDbContext.Employees.FirstOrDefaultAsync(Q => Q.EmployeeIdentity == id);


                _employeeDbContext.Employees.Remove(toBeDeleted);

                await _employeeDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task RemoveEmployeeAsync(int id)
        {
            try
            {
                var toBeDeleted = await _employeeDbContext.Employees.FirstOrDefaultAsync(Q => Q.EmployeeIdentity == id);

                toBeDeleted.IsActive = false;

                _employeeDbContext.Employees.Update(toBeDeleted);

                await _employeeDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
