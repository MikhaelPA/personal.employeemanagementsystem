﻿using Employee.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Services
{
    public class EmailService
    {
        private readonly SmtpClient _client;
        private readonly AppSettings _appSettings;
        public EmailService(SmtpClient client,
            AppSettings appSettings)
        {
            _client = client;
            _appSettings = appSettings;
        }

        public async Task<bool> SendEmailAsync(string recipient, string bodyMessage, string subject)
        {
            _client.Credentials = new NetworkCredential
            {
                UserName = _appSettings.Email.Smtp.Username,
                Password = _appSettings.Email.Smtp.Password,
            };
            _client.EnableSsl = true;
            try
            {
                //body email
                string html = $"<html>{bodyMessage}</html>";
                var message = new MailMessage(_appSettings.Email.Admin, recipient)
                {
                    IsBodyHtml = true,
                    Subject = subject,
                    Body = html
                };
                await _client.SendMailAsync(message);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex,"Error when sending EMAIL");
                return false;
            }
        }
    }
}
